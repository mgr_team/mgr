

import numpy, sys
import subprocess
sys.path.append("C:/git/mgr")
import mgr.src.mgrlib as mgrlib
from snappy import jpy
import add_pixels_to_final_product

Color = jpy.get_type('java.awt.Color')
ColorPoint = jpy.get_type('org.esa.snap.core.datamodel.ColorPaletteDef$Point')
ColorPaletteDef = jpy.get_type('org.esa.snap.core.datamodel.ColorPaletteDef')
ImageInfo = jpy.get_type('org.esa.snap.core.datamodel.ImageInfo')
ImageLegend = jpy.get_type('org.esa.snap.core.datamodel.ImageLegend')
ImageManager = jpy.get_type('org.esa.snap.core.image.ImageManager')
JAI = jpy.get_type('javax.media.jai.JAI')
RenderedImage = jpy.get_type('java.awt.image.RenderedImage')


def getCurrentPathWith(path):
    import os
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), path)

def resetFinalProduct(finalProductPath):
    from snappy  import HashMap, GPF

    product = mgrlib.readProduct(finalProductPath)
    if product.getBand('frequencies'):
        return

    BandDescriptor = jpy.get_type(
        'org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor')
    band = BandDescriptor()
    band.name = 'frequencies'
    band.type = 'float32'
    band.expression = "0"

    targetBands = jpy.array(
        'org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor', 1)
    targetBands[0] = band

    params = HashMap()
    params.put('targetBands', targetBands)
    result = GPF.createProduct('BandMaths', params, product)
    mgrlib.saveProduct(result, finalProductPath)
    

def generateRGBs(imagePaths):
    for imagePath in imagePaths:
        try:
            # add_pixels_to_final_product.process(finalProductPath, imagePath)
            args = ["py", "C:/git/mgr/mgr/src/thesis/generate_RGB_from_image.py", 
            "-imagePath",  imagePath]
            fail = subprocess.call(args)

            if fail:
                raise Exception("")    
            mgrlib.Logger.log("Generated RGB from " + imagePath)

        except Exception as e: 
            mgrlib.Logger.error("Error occured while counting watered pixels for product: {} \n{}".format(imagePath, e))
            return None 
        
imagesPaths = mgrlib.readImagesPaths(getCurrentPathWith("images_lists/all.txt"))
generateRGBs(imagesPaths)

