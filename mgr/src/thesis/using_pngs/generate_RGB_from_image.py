import numpy, sys
from argparse import ArgumentParser
sys.path.append("C:/git/mgr")
import mgr.src.mgrlib as mgrlib
from snappy import jpy

vectorPath = 'C:/git/mgr/mgr/src/correlation/resources/geometry_test_Polygon.shp'

Color = jpy.get_type('java.awt.Color')
ColorPoint = jpy.get_type('org.esa.snap.core.datamodel.ColorPaletteDef$Point')
ColorPaletteDef = jpy.get_type('org.esa.snap.core.datamodel.ColorPaletteDef')
ImageInfo = jpy.get_type('org.esa.snap.core.datamodel.ImageInfo')
ImageLegend = jpy.get_type('org.esa.snap.core.datamodel.ImageLegend')
ImageManager = jpy.get_type('org.esa.snap.core.image.ImageManager')
JAI = jpy.get_type('javax.media.jai.JAI')
RenderedImage = jpy.get_type('java.awt.image.RenderedImage')

tempProductBandName = "tempBand"

def generateRGBFromImage(product, rgbDir):
    from snappy import ProductIO, ProductUtils, String
    band = product.getBand(tempProductBandName)

    points = [ColorPoint(0.0, Color.WHITE), 
        ColorPoint(0.1, Color.BLACK)]

    cpd = ColorPaletteDef(points)
    ii = ImageInfo(cpd)
    band.setImageInfo(ii)
    image_format = 'PNG'
    im = ImageManager.getInstance().createColoredBandImage(
        [band], band.getImageInfo(), 0)

    rgbPath = rgbDir + "/" + product.getName() + ".png"
    JAI.create("filestore", im,  rgbPath, image_format)
    print("Image saved to: " + rgbPath)
    product.closeIO()

def preprocessProduct(product):
    from snappy import HashMap, GPF
    import os
    result = mgrlib.addVectorToProduct(product, ["dummy"], vectorPath)

    BandDescriptor = jpy.get_type(
        'org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor')
    band = BandDescriptor()
    band.name = tempProductBandName
    band.type = 'float32'
    band.expression = "geometry_test_Polygon && Sigma0_VV > 0 && Sigma0_VV < 0.012 ? 1 : 0"

    targetBands = jpy.array(
        'org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor', 1)
    targetBands[0] = band

    params = HashMap()
    params.put('targetBands', targetBands)
    result = GPF.createProduct('BandMaths', params, result)
    return result

def process(imagePath, rgbDir):
    from snappy import ProductIO, ProductUtils, String
  
    product = mgrlib.readProduct(imagePath)
    result = preprocessProduct(product)
    generateRGBFromImage(result, rgbDir)

def main():
    parser = ArgumentParser()
    parser.add_argument("-imagePath",  help="Path to source image", required=True)
    parser.add_argument("-finalProductPath",  help="Path to source image", required=True)

    args = parser.parse_args()
    process(args.finalProductPath, args.imagePath)

if __name__ == "__main__":
    main()