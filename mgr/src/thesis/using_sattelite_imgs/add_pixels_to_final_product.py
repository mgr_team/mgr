import numpy, sys
from argparse import ArgumentParser
sys.path.append("C:/git/mgr")
import mgr.src.mgrlib as mgrlib
from snappy import jpy

tempProductPath = "e:/mgr/images/thesis/inundation_frequency/image/temp/temp.dim"
tempDirPath = "e:/mgr/images/thesis/inundation_frequency/image/temp"
tempProductBandName = "tempBand"
vectorPath = 'C:/git/mgr/mgr/src/correlation/resources/geometry_test_Polygon.shp'
finalProductPath = "e:/mgr/images/thesis/inundation_frequency/image/20181118_orbit_cal_VV_crop_spk_tc_Sigma0_VV_reprojected.dim"


def removeTempProductIfExists():
    try:
        import shutil
        shutil.rmtree(tempDirPath)
       
    except:
        # ignore
        ignore = True

def saveToTempProductBandWithWateredPoly(imagePath):
    from snappy  import HashMap, GPF
    import os

    removeTempProductIfExists()
    product = mgrlib.readProduct(imagePath)
    result = mgrlib.addVectorToProduct(product, ["dummy"], vectorPath)

    BandDescriptor = jpy.get_type(
        'org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor')
    band = BandDescriptor()
    band.name = tempProductBandName
    band.type = 'float32'
    band.expression = "geometry_test_Polygon && Sigma0_VV > 0 && Sigma0_VV < 0.012 ? 1 : 0"

    targetBands = jpy.array(
        'org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor', 1)
    targetBands[0] = band

    params = HashMap()
    params.put('targetBands', targetBands)
    result = GPF.createProduct('BandMaths', params, result)
    
    mgrlib.saveProduct(result, tempProductPath)


def process(finalProductPath, imagePath):
    from snappy import ProductIO, ProductUtils, String

    saveToTempProductBandWithWateredPoly(imagePath)
    tempProduct = mgrlib.readProduct(tempProductPath)
    imageBand = tempProduct.getBand(tempProductBandName)

    finalProduct = mgrlib.readProduct(finalProductPath)
    finalProductBand = finalProduct.getBand('frequencies')
   
    finalProductBandWidth = finalProduct.getSceneRasterWidth()
    finalProductBandHeight = finalProduct.getSceneRasterHeight()
    
    writer = ProductIO.getProductWriter('BEAM-DIMAP')
    ProductUtils.copyGeoCoding(finalProduct, finalProduct)    
    finalProduct.setProductWriter(writer)
    finalProduct.writeHeader(String(finalProductPath))
    finalProductBand.setNoDataValue(0)
    finalProductBand.setNoDataValueUsed(True)

    width = tempProduct.getSceneRasterWidth()
    height = tempProduct.getSceneRasterHeight()
    tempImagePixels =  numpy.zeros(width, dtype=numpy.float32)
    imagePixels =  numpy.zeros(width, dtype=numpy.float32)
    finalProductPixels =  numpy.zeros(finalProductBandWidth, dtype=numpy.float32)

    for y in range(height):
        if y >= finalProductBandHeight:
            continue

        imageBand.readPixels(0, y, width, 1, tempImagePixels)
        finalProductBand.readPixels(0, y, finalProductBandWidth, 1, finalProductPixels)

        if finalProductBandWidth != width:
            imagePixels = numpy.resize(tempImagePixels, (1, finalProductBandWidth))
            finalProductPixels = numpy.resize(finalProductPixels, (1, finalProductBandWidth))
            finalProductPixels += imagePixels > 0
        else:
            finalProductPixels += tempImagePixels > 0

        finalProductBand.writePixels(0, y, finalProductBandWidth, 1, finalProductPixels)
    finalProduct.closeIO()

# imagePath = 'e:/mgr/images/processed/153_VV_full/20141004/20141004_orbit_cal_mosaic_crop_spk_tc_Sigma0_VV_reprojected.dim'
# finalProductPath = "e:/mgr/images/thesis/inundation_frequency/image/20181118_orbit_cal_VV_crop_spk_tc_Sigma0_VV_reprojected.dim"

# process(finalProductPath, imagePath) 
def main():
    parser = ArgumentParser()
    parser.add_argument("-imagePath",  help="Path to source image", required=True)
    parser.add_argument("-finalProductPath",  help="Path to source image", required=True)

    args = parser.parse_args()
    process(args.finalProductPath, args.imagePath)

if __name__ == "__main__":
    main()