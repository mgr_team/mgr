

import numpy, sys
import subprocess
sys.path.append("C:/git/mgr")
import mgr.src.mgrlib as mgrlib
from snappy import jpy
import add_pixels_to_final_product

Color = jpy.get_type('java.awt.Color')
ColorPoint = jpy.get_type('org.esa.snap.core.datamodel.ColorPaletteDef$Point')
ColorPaletteDef = jpy.get_type('org.esa.snap.core.datamodel.ColorPaletteDef')
ImageInfo = jpy.get_type('org.esa.snap.core.datamodel.ImageInfo')
ImageLegend = jpy.get_type('org.esa.snap.core.datamodel.ImageLegend')
ImageManager = jpy.get_type('org.esa.snap.core.image.ImageManager')
JAI = jpy.get_type('javax.media.jai.JAI')
RenderedImage = jpy.get_type('java.awt.image.RenderedImage')

outputRGBDir = "e:/mgr/images/thesis/inundation_frequency"
def getCurrentPathWith(path):
    import os
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), path)

def resetFinalProduct(finalProductPath):
    args = ["py", "C:/git/mgr/mgr/src/thesis/using_sattelite_imgs/reset_final_product.py",
    "-finalProductPath",  finalProductPath]
    fail = subprocess.call(args)
    if fail:
        mgrlib.Logger.error("Error occured while resetting final product")


def generateRGBFromFinalProduct(finalProductPath, rgbDir, rgbName):
  
    args = ["py", "C:/git/mgr/mgr/src/thesis/using_sattelite_imgs/generate_RGB_from_final_product.py",
    "-finalProductPath",  finalProductPath,
    "-rgbDir", rgbDir,
    "-rgbName", rgbName]
    fail = subprocess.call(args)
    if fail:
        mgrlib.Logger.error("Error occured while generation RGB from final product")

def addSigmasToFinalProduct(imagePaths, finalProductPath):
    cnt = 1
    for imagePath in imagePaths:
        print("{}/{}".format(cnt, len(imagePaths)))
        try:
            # add_pixels_to_final_product.process(finalProductPath, imagePath)
            args = ["py", "C:/git/mgr/mgr/src/thesis/using_sattelite_imgs/add_pixels_to_final_product.py", 
            "-imagePath",  imagePath,
            "-finalProductPath", finalProductPath]
            fail = subprocess.call(args)

            if fail:
                raise Exception("")    
            mgrlib.Logger.log("Processed image " + imagePath)
            generateRGBFromFinalProduct(finalProductPath, outputRGBDir, str(cnt))
            cnt = cnt + 1

        except Exception as e: 
            mgrlib.Logger.error("Error occured while counting watered pixels for product: {} \n{}".format(imagePath, e))
            return None 
        
imagesPaths = mgrlib.readImagesPaths(getCurrentPathWith("../images_lists/all.1.txt"))
finalProductPath = "e:/mgr/images/thesis/inundation_frequency/image/20181118_orbit_cal_VV_crop_spk_tc_Sigma0_VV_reprojected.dim"

# resetFinalProduct(finalProductPath)
# addSigmasToFinalProduct(imagesPaths, finalProductPath)

generateRGBFromFinalProduct(finalProductPath, outputRGBDir, "final_yellow_2")