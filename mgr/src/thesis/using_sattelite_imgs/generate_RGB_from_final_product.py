import numpy, sys
from argparse import ArgumentParser
sys.path.append("C:/git/mgr")
import mgr.src.mgrlib as mgrlib
from snappy import jpy

vectorPath = 'C:/git/mgr/mgr/src/correlation/resources/geometry_test_Polygon.shp'

Color = jpy.get_type('java.awt.Color')
ColorPoint = jpy.get_type('org.esa.snap.core.datamodel.ColorPaletteDef$Point')
ColorPaletteDef = jpy.get_type('org.esa.snap.core.datamodel.ColorPaletteDef')
ImageInfo = jpy.get_type('org.esa.snap.core.datamodel.ImageInfo')
ImageLegend = jpy.get_type('org.esa.snap.core.datamodel.ImageLegend')
ImageManager = jpy.get_type('org.esa.snap.core.image.ImageManager')
JAI = jpy.get_type('javax.media.jai.JAI')
RenderedImage = jpy.get_type('java.awt.image.RenderedImage')

tempProductBandName = "tempBand"

def generateRGBFromFinalProduct(finalProductPath, rgbDir, rgbName):
    from snappy import ProductIO, ProductUtils, String
    product = mgrlib.readProduct(finalProductPath)
    band = product.getBand('frequencies')
 
    points = [ColorPoint(0, Color.WHITE), 
        ColorPoint(10, Color.YELLOW), 
        ColorPoint(20, Color.ORANGE),
        ColorPoint(30, Color.GREEN), 
        ColorPoint(50, Color.RED)]

    cpd = ColorPaletteDef(points)
    ii = ImageInfo(cpd)
    band.setImageInfo(ii)
    image_format = 'PNG'
    im = ImageManager.getInstance().createColoredBandImage(
        [band], band.getImageInfo(), 0)

    rgbPath = rgbDir + "/" + rgbName + ".png"
    JAI.create("filestore", im,  rgbPath, image_format)

    legend = ImageLegend(band.getImageInfo(), band)
    legend.setHeaderText(band.getName())
    legend.setOrientation(ImageLegend.VERTICAL) # or ImageLegend.VERTICAL
    legend_image = legend.createImage()
    rendered_legend_image = jpy.cast(legend_image, RenderedImage)
    legendPath =  "{}/{}_legend.png".format(rgbDir, rgbName)
    JAI.create("filestore", rendered_legend_image, legendPath, image_format)

    print("Image saved to: " + rgbPath)

def main():
    parser = ArgumentParser()
    parser.add_argument("-finalProductPath",  help="Path to source image", required=True)
    parser.add_argument("-rgbDir",  help="Path to source image", required=True)
    parser.add_argument("-rgbName",  help="Path to source image", required=True)

    args = parser.parse_args()
    generateRGBFromFinalProduct(args.finalProductPath, args.rgbDir, args.rgbName)

if __name__ == "__main__":
    main()