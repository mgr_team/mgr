import sys
from argparse import ArgumentParser
sys.path.append("C:/git/mgr")
import mgr.src.mgrlib as mgrlib
from snappy import jpy

def resetFinalProduct(finalProductPath):
    from snappy  import HashMap, GPF

    product = mgrlib.readProduct(finalProductPath)
    band = product.getBand('frequencies')
    if band:
        product.removeBand(band)

    BandDescriptor = jpy.get_type(
        'org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor')
    band = BandDescriptor()
    band.name = 'frequencies'
    band.type = 'float32'
    band.expression = "0"

    targetBands = jpy.array(
        'org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor', 1)
    targetBands[0] = band

    params = HashMap()
    params.put('targetBands', targetBands)
    result = GPF.createProduct('BandMaths', params, product)
    mgrlib.saveProduct(result, finalProductPath)


def main():
    parser = ArgumentParser()
    parser.add_argument("-finalProductPath",  help="Path to source image", required=True)
    
    args = parser.parse_args()
    resetFinalProduct(args.finalProductPath)

if __name__ == "__main__":
    main()