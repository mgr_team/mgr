# outFileName is an array, because array is inmutable and can be
# used as referenced type - first element will change after exiting
# methods


import os
import datetime

def printMemoryUsage():
    Logger.log(getMemoryUsage())
    
def getMemoryUsage():
    import psutil
    process = psutil.Process(os.getpid())
    memoryGB = int(process.memory_info().rss) / (1 << 30)
    return "Memory usage: {} GB".format(memoryGB)
    
def processImage(product, outImageName, sourceBands):
    result = applySpeckleFiltering(product, outImageName, sourceBands)
    # Logger.log("Finished applying speckle filtering")
    result = applyTerrainCorrection(result, outImageName, sourceBands)
    # Logger.log("Finished applying terrain correction")
    result = reprojectToEPSG2180(result, outImageName)
    # Logger.log("Finished reprojecting to EPSG 2180")
    return result

def calibrateProduct(source, outFileName, pol):
    from snappy import HashMap, GPF

    params = HashMap()
    params.put('outputSigmaBand', True)
#   params.put('sourceBands', 'Intensity_' + pol)
    params.put('selectedPolarisations', pol)
    params.put('outputImageScaleInDb', False)
    calibrated = GPF.createProduct("Calibration", params, source)
    outFileName[0] += "_cal_{}".format(pol)
    return calibrated


def cropProduct(source, outFileName, wkt):
    from snappy import jpy, HashMap, GPF

    WKTReader = jpy.get_type('com.vividsolutions.jts.io.WKTReader')
    geom = WKTReader().read(wkt)
    params = HashMap()
    params.put('geoRegion', geom)
    params.put('outputImageScaleInDb', False)
    cropped = GPF.createProduct("Subset", params, source)
    outFileName[0] += "_crop"
    return cropped


def applyTerrainCorrection(source, outFileName, sourceBands):
    from snappy import HashMap, GPF

    params = HashMap()
    params.put('demResamplingMethod', 'BILINEAR_INTERPOLATION')
    params.put('imgResamplingMethod', 'BILINEAR_INTERPOLATION')
    params.put('demName', 'SRTM 3Sec')
    params.put('pixelSpacingInMeter', 10.0)
    params.put('sourceBands', sourceBands)
    terrainCorrected = GPF.createProduct("Terrain-Correction", params, source)
    outFileName[0] += "_tc_" + sourceBands
    return terrainCorrected


def assembleMultipleSlices(productsToAssemble, outFileName, pol):
    from snappy import HashMap, GPF, jpy
    products = jpy.array(
        'org.esa.snap.core.datamodel.Product', len(productsToAssemble))

    for i, product in enumerate(productsToAssemble):
        products[i] = product

    params = HashMap()
    params.put('selectedPolarisations', pol)
    params.put('sourceProducts', products)

    outFileName[0] += "_mosaic"
    result = GPF.createProduct("SliceAssembly", params, products)

    return result


def assembleTwoSlices(product1, product2, outFileName, pol):
    from snappy import HashMap, GPF, jpy

    products = jpy.array('org.esa.snap.core.datamodel.Product', 2)
    products[0] = product1
    products[1] = product2

    params = HashMap()
    params.put('selectedPolarisations', pol)
    params.put('sourceProducts', products)

    outFileName[0] += "_mosaic"
    result = GPF.createProduct("SliceAssembly", params, products)

    return result


def applySpeckleFiltering(product, outFileName, sourceBands):
    from snappy import HashMap, GPF
    outFileName[0] += "_spk"

    params = HashMap()
    params.put('sourceBands', sourceBands)
    params.put('filter', 'Lee')
    params.put('filterSizeX', '3')
    params.put('filterSizeY', '3')
    params.put('estimateENL', 'true')
    params.put('windowSize', '7x7')

    result = GPF.createProduct("Speckle-Filter", params, product)
    return result


def applyOrbitFile(source, outFileName):
    from snappy import HashMap, GPF

    params = HashMap()
    params.put('orbitType', 'Sentinel Precise (Auto Download)')
    out = GPF.createProduct("Apply-Orbit-File", params, source)
    outFileName[0] += "_orbit"
    return out

def reprojectToEPSG2180(product, outFileName):
    from snappy import HashMap, GPF

    params = HashMap()
    params.put('crs', "EPSG:2180")
    params.put('resampling', "Nearest")
    params.put('orthorectify', "false")
    params.put('includeTiePointGrids', "true")

    result = GPF.createProduct("Reproject", params, product)
    outFileName[0] += "_reprojected"
    return result


def readProduct(src):
    from snappy import ProductIO
    return ProductIO.readProduct(src)


def saveProduct(product, path):
    from snappy import ProductIO
    ProductIO.writeProduct(product, path, 'BEAM-DIMAP')


def saveProduct2(product, dirPath, fileName):
    import os
    path = os.path.join(dirPath, fileName)
    if not os.path.exists(dirPath):
        os.makedirs(dirPath)
    saveProduct(product, path)


def getFileName(filePath):
    import os
    base = os.path.basename(filePath)
    return os.path.splitext(base)[0]


def readImagesPaths(filename):
    with open(filename) as file:
        content = file.read().splitlines()
        return content


def saveImagesPathsToFile(filename, imagesPaths):
    with open(filename, 'w') as file:
        for path in imagesPaths:
            file.write("{}\n".format(path))

def addVectorToProduct(source, outFileName, vectorPath):
    from snappy import GPF, HashMap
    # To build from wkt see snappy examples -> snappy_geo_roi
    params = HashMap()
    params.put('vectorFile', vectorPath)
    params.put('separateShapes', False)
    result = GPF.createProduct("Import-Vector", params, source)
    outFileName[0] += "_vec"
    
    return result


class ProcessingItemsCounter:
    def __init__(self, n, logger, postfix=""):
        self.cnt = 1
        self.n = n
        self.postfix = postfix
        self.logger = logger

    def increaseCounter(self):
        self.cnt += 1

    def printCountingMessage(self):
        Logger.log("{} {}".format(self.getCountingMessage(), self.postfix))

    def getCountingMessage(self):
        return "{}/{}".format(self.cnt, self.n)


class ExtendedLogger:
    def __init__(self, dir, postfix=""):
        self.filename = "{}{}.csv".format(
            datetime.datetime.now().strftime('%Y-%m-%d_%H-%M'), postfix)
        self.filepath = os.path.join(dir, self.filename)

    def error(self, message):
        from termcolor import colored
        message = self.getMessageWithDateNowAppendix("ERROR " + message)
        print(colored(message, 'red'))
        self.writeToFile(message)

    def log(self, message):
        message = self.getMessageWithDateNowAppendix(message)
        print(message)
        self.writeToFile(message)

    def getMessageWithDateNowAppendix(self, message):
        import datetime
        message = "{} {}".format(
            datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S'), message)
        return message
        

    def logWithoutDate(self, message):
        print(message)
        self.writeToFile(message)

    def writeToFile(self, message):
        dirname = os.path.dirname(self.filepath)
        if not os.path.exists(dirname):
            os.makedirs(dirname)
        with open(self.filepath, 'a') as logFile:
            logFile.write(message + "\n")


class Logger:
    @staticmethod
    def error(message):
        Logger.printMessageWithDateNowAppendix("ERROR " + message)

    @staticmethod
    def log(message):
        Logger.printMessageWithDateNowAppendix(message)

    @staticmethod
    def printMessageWithDateNowAppendix(message):
        import datetime
        print("{} {}".format(datetime.datetime.now().strftime(
            '%Y-%m-%d %H:%M:%S'), message))

    @staticmethod
    def logWithoutDate(message):
        print(message)


class CsvLogger:
    def __init__(self, dir, postfix=""):
        self.filename = "{}{}.csv".format(
            datetime.datetime.now().strftime('%Y-%m-%d_%H-%M'), postfix)
        self.filepath = os.path.join(dir, self.filename)

    def appendRow(self, row):
        with open(self.filepath, 'a') as logFile:
            logFile.write(row + "\n")

    @staticmethod
    def appendRowToFile(filepath, row):
        with open(filepath, 'a') as logFile:
            logFile.write(row + "\n")

    @staticmethod
    def writeRowToFile(filepath, row):
        with open(filepath, 'w+') as logFile:
            logFile.write(row + "\n")

def readLines(path):
    with open(path) as file:
        return file.read().splitlines()

def extractDateFromImagePath(imagePath):
    import re
    return re.findall("\d{8}", imagePath)[0]
