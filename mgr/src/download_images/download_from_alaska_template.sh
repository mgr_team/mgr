#!/bin/bash
declare -a arr=(

"https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190806T161945_20190806T162010_028451_03371A_1534.zip"
)

for i in "${arr[@]}"
 do
		wget   --continue --user=jakub.osowicki --password=<passwd> "${i}"  --tries=10 
	echo "Downloading $i"
	sleep 60
done