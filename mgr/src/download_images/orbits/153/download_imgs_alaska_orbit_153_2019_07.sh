#!/bin/bash
declare -a arr=(

                 # "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190716T044309_20190716T044334_017154_020453_8BA1.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190710T044350_20190710T044415_028050_032AF1_4D28.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190704T044308_20190704T044333_016979_01FF27_35E6.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190628T044349_20190628T044414_027875_03259D_E34D.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190622T044308_20190622T044333_016804_01F9F8_007F.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190616T044349_20190616T044414_027700_032065_49E8.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190610T044307_20190610T044332_016629_01F4C6_3883.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190604T044348_20190604T044413_027525_031B1C_102B.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190529T044306_20190529T044331_016454_01EF8F_2E31.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190523T044348_20190523T044413_027350_0315AE_33BA.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190517T044305_20190517T044330_016279_01EA30_4C48.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190511T044347_20190511T044412_027175_031036_0FF4.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190505T044304_20190505T044329_016104_01E4B7_4CD2.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190429T044347_20190429T044412_027000_030A28_EE7C.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190423T044304_20190423T044329_015929_01DEDB_8912.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190417T044346_20190417T044411_026825_0303D7_2D28.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190411T044303_20190411T044328_015754_01D90F_846E.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190405T044346_20190405T044411_026650_02FD7A_672B.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190330T044303_20190330T044328_015579_01D33F_F0C3.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190324T044345_20190324T044410_026475_02F702_0B4E.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190318T044303_20190318T044328_015404_01CD84_DCFE.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190312T044345_20190312T044410_026300_02F08B_9BE1.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190306T044303_20190306T044328_015229_01C7DB_2E99.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190228T044345_20190228T044410_026125_02EA2F_C104.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190222T044306_20190222T044331_015054_01C212_2AD4.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190216T044345_20190216T044410_025950_02E3EF_6482.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190210T044303_20190210T044328_014879_01BC59_BE77.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190204T044345_20190204T044410_025775_02DDC0_B284.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190129T044303_20190129T044328_014704_01B69C_E875.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190123T044346_20190123T044411_025600_02D765_5B55.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190117T044304_20190117T044329_014529_01B0FC_A4F4.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20190111T044346_20190111T044411_025425_02D102_9CB7.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20190105T044304_20190105T044329_014354_01AB56_7953.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20181230T044347_20181230T044412_025250_02CAB8_FD53.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20181224T044305_20181224T044330_014179_01A599_D009.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20181218T044347_20181218T044412_025075_02C45E_88B6.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20181212T044305_20181212T044330_014004_019FCE_AD0F.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20181130T044305_20181130T044330_013829_019A22_2A86.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20181124T044348_20181124T044413_024725_02B828_9F10.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20181118T044306_20181118T044331_013654_01948C_F558.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20181112T044348_20181112T044413_024550_02B1B8_5B92.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20181106T044306_20181106T044331_013479_018F09_5B8E.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20181031T044348_20181031T044413_024375_02AB5B_80E6.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20181025T044307_20181025T044332_013304_018989_5A9F.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20181019T044349_20181019T044414_024200_02A5B4_F5E6.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20181013T044307_20181013T044332_013129_018422_D0DE.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20181007T044348_20181007T044413_024025_02A000_F543.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20181001T044306_20181001T044331_012954_017ED1_7B67.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180925T044348_20180925T044413_023850_029A47_C9F9.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180919T044306_20180919T044331_012779_017971_71DF.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180913T044348_20180913T044413_023675_029492_216A.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180907T044306_20180907T044331_012604_01741A_66A0.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180901T044347_20180901T044412_023500_028EFC_FA2A.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180826T044305_20180826T044330_012429_016EB2_3778.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180820T044347_20180820T044412_023325_02896E_3D2E.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180814T044304_20180814T044329_012254_016944_F481.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180808T044346_20180808T044411_023150_0283C3_3C5E.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180802T044304_20180802T044329_012079_0163DC_A78B.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180727T044345_20180727T044410_022975_027E48_595C.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180721T044303_20180721T044328_011904_015E97_030D.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180715T044345_20180715T044410_022800_0278C1_0412.zip",
        #                "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180709T044302_20180709T044327_011729_015939_8470.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180703T044344_20180703T044409_022625_027382_0FAA.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180627T044302_20180627T044327_011554_0153CA_659A.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180621T044343_20180621T044408_022450_026E6A_F250.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180615T044301_20180615T044326_011379_014E5C_76E9.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180609T044342_20180609T044407_022275_026919_4300.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180603T044311_20180603T044336_011204_0148FF_FD48.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180528T044353_20180528T044418_022100_02639E_C51E.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180522T044302_20180522T044327_011029_014353_335C.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180516T044352_20180516T044417_021925_025E0F_7AF1.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180510T044301_20180510T044326_010854_013DA4_1F6D.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180504T044351_20180504T044416_021750_025872_05CA.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180428T044300_20180428T044325_010679_0137FF_C52F.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180422T044351_20180422T044416_021575_0252EF_0EB2.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180416T044300_20180416T044325_010504_013264_A5BA.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180410T044350_20180410T044415_021400_024D7E_6DF1.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180404T044300_20180404T044325_010329_012CCD_B305.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180329T044341_20180329T044406_021225_024800_1CDB.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180323T044259_20180323T044324_010154_01271B_A0E8.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180317T044350_20180317T044415_021050_024273_051A.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180311T044259_20180311T044324_009979_01216B_EA54.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180305T044340_20180305T044405_020875_023CE4_A467.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180227T044259_20180227T044324_009804_011B8E_748E.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180221T044340_20180221T044405_020700_02375B_7404.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180215T044259_20180215T044324_009629_0115BF_DD3A.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180209T044340_20180209T044405_020525_0231CB_A4A2.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180203T044259_20180203T044324_009454_010FFD_2C28.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180128T044341_20180128T044406_020350_022C32_3F8D.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180122T044259_20180122T044324_009279_010A3F_CA1A.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180116T044341_20180116T044406_020175_0226A7_2BDE.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20180110T044300_20180110T044325_009104_01048D_BD53.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20180104T044341_20180104T044406_020000_022115_296B.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20171229T044300_20171229T044325_008929_00FED8_202B.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20171217T044301_20171217T044326_008754_00F940_E7EF.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20171205T044259_20171205T044324_008579_00F3AC_AFB2.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20171129T044343_20171129T044408_019475_0210BC_FDF0.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20171123T044309_20171123T044334_008404_00EE22_37D6.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20171117T044343_20171117T044408_019300_020B33_AE6C.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20171111T044310_20171111T044335_008229_00E8D7_A16E.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20171105T044343_20171105T044408_019125_0205BD_C8A2.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20171030T044310_20171030T044335_008054_00E3B6_AF48.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20171024T044343_20171024T044408_018950_020064_4757.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20171018T044310_20171018T044335_007879_00DEAB_93F3.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20171012T044343_20171012T044408_018775_01FB06_0AA5.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170924T044310_20170924T044335_007529_00D4AD_5DCD.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170918T044343_20170918T044408_018425_01F059_CD97.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170912T044309_20170912T044334_007354_00CF9E_7FB8.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170831T044309_20170831T044334_007179_00CA78_4E3F.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170819T044308_20170819T044333_007004_00C56F_243D.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170807T044308_20170807T044333_006829_00C051_D913.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170726T044307_20170726T044332_006654_00BB45_4E06.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170714T044306_20170714T044331_006479_00B646_C558.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170708T044339_20170708T044404_017375_01D048_12E4.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170702T044305_20170702T044330_006304_00B159_8E9F.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170626T044339_20170626T044404_017200_01CB06_8A4D.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170620T044305_20170620T044330_006129_00AC47_D230.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170614T044338_20170614T044403_017025_01C5B1_5621.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170608T044304_20170608T044329_005954_00A725_8380.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170602T044337_20170602T044402_016850_01C046_E427.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170527T044311_20170527T044336_005779_00A214_2B81.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170527T044246_20170527T044311_005779_00A214_B5F8.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170521T044352_20170521T044417_016675_01BAE1_2DF4.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170521T044327_20170521T044352_016675_01BAE1_D81B.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170515T044311_20170515T044336_005604_009D0F_49F3.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170515T044246_20170515T044311_005604_009D0F_1649.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170427T044353_20170427T044418_016325_01B030_8CB1.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170427T044328_20170427T044353_016325_01B030_068F.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170415T044352_20170415T044417_016150_01AAE0_FB7D.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170415T044327_20170415T044352_016150_01AAE0_4183.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170409T044253_20170409T044318_005079_008E2E_546A.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170403T044352_20170403T044417_015975_01A587_9F05.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170403T044327_20170403T044352_015975_01A587_54D4.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170328T044253_20170328T044318_004904_008921_C06A.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170322T044351_20170322T044416_015800_01A059_C26C.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170322T044326_20170322T044351_015800_01A059_6C7A.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170316T044252_20170316T044317_004729_008423_EFB1.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170310T044351_20170310T044416_015625_019B22_0E46.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170310T044326_20170310T044351_015625_019B22_430B.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170304T044252_20170304T044317_004554_007EF8_BBD7.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170226T044351_20170226T044416_015450_0195D9_55BF.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170226T044326_20170226T044351_015450_0195D9_638A.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170220T044252_20170220T044317_004379_0079D8_A046.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170214T044351_20170214T044416_015275_019081_24D7.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170214T044326_20170214T044351_015275_019081_5FAA.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170208T044252_20170208T044317_004204_007491_84B1.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170202T044351_20170202T044416_015100_018AFA_09E3.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170202T044326_20170202T044351_015100_018AFA_E21F.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170127T044253_20170127T044318_004029_006F63_409E.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170121T044352_20170121T044417_014925_0185B7_855F.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170121T044327_20170121T044352_014925_0185B7_390A.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170115T044253_20170115T044318_003854_006A21_6635.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170109T044352_20170109T044417_014750_018035_5437.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20170109T044327_20170109T044352_014750_018035_FD5C.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170103T044310_20170103T044335_003679_00650A_5A4A.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20170103T044245_20170103T044310_003679_00650A_958A.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20161228T044354_20161228T044419_014575_017AE8_0FEA.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20161228T044329_20161228T044354_014575_017AE8_6A18.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20161222T044259_20161222T044328_003504_005FDF_93E0.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20161216T044354_20161216T044419_014400_01757A_4172.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20161216T044329_20161216T044354_014400_01757A_BE22.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20161210T044259_20161210T044328_003329_005AF0_4D22.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20161204T044354_20161204T044419_014225_016FEB_DE8E.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20161204T044329_20161204T044354_014225_016FEB_AB55.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20161128T044300_20161128T044329_003154_0055DE_80EE.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20161122T044355_20161122T044420_014050_016A7A_ACE2.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20161122T044330_20161122T044355_014050_016A7A_F41C.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20161116T044300_20161116T044329_002979_005100_05B4.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20161110T044355_20161110T044420_013875_016523_F852.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20161110T044330_20161110T044355_013875_016523_6606.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20161104T044300_20161104T044329_002804_004C02_C76B.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20161029T044355_20161029T044420_013700_015FA0_7BAE.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20161029T044330_20161029T044355_013700_015FA0_1066.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20161023T044312_20161023T044337_002629_004721_1C2C.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20161023T044247_20161023T044312_002629_004721_E10F.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20161017T044355_20161017T044420_013525_015A39_1961.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20161017T044330_20161017T044355_013525_015A39_8E94.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20161011T044312_20161011T044337_002454_00424B_F0F8.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20161011T044247_20161011T044312_002454_00424B_27C3.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20161005T044355_20161005T044420_013350_0154AD_6610.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20161005T044330_20161005T044355_013350_0154AD_7BCC.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20160929T044312_20160929T044337_002279_003D80_8BBF.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SB/S1B_IW_GRDH_1SDV_20160929T044247_20160929T044312_002279_003D80_78E6.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160923T044355_20160923T044420_013175_014F20_BA72.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160923T044330_20160923T044355_013175_014F20_4802.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160911T044354_20160911T044419_013000_014951_004E.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160911T044329_20160911T044354_013000_014951_978C.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160830T044354_20160830T044419_012825_0143AD_D942.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160830T044329_20160830T044354_012825_0143AD_7D0D.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160818T044354_20160818T044419_012650_013DC1_B9E5.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160818T044329_20160818T044354_012650_013DC1_7A9C.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160806T044353_20160806T044418_012475_013800_041A.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160806T044328_20160806T044353_012475_013800_22C6.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160725T044352_20160725T044417_012300_01322D_4F74.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160725T044327_20160725T044352_012300_01322D_6DED.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160713T044351_20160713T044416_012125_012C78_E842.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160713T044326_20160713T044351_012125_012C78_0C66.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160701T044351_20160701T044416_011950_0126BD_BE1D.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160701T044326_20160701T044351_011950_0126BD_D80D.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160607T044350_20160607T044415_011600_011BAD_EC1F.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160607T044325_20160607T044350_011600_011BAD_FF9C.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160526T044329_20160526T044354_011425_011622_7DC3.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160514T044329_20160514T044354_011250_011063_2E97.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160502T044328_20160502T044353_011075_010AD7_6F75.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160408T044327_20160408T044352_010725_01000A_8620.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160327T044326_20160327T044351_010550_00FAE5_850A.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160303T044326_20160303T044351_010200_00F0ED_0F2C.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160220T044326_20160220T044351_010025_00EBFB_4852.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160208T044326_20160208T044351_009850_00E6CB_C640.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160127T044326_20160127T044351_009675_00E1C8_2422.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160115T044326_20160115T044351_009500_00DCA6_EC44.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20160103T044327_20160103T044352_009325_00D7A6_E657.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20151222T044327_20151222T044352_009150_00D2A8_3584.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20151128T044328_20151128T044353_008800_00C8DA_9D2C.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20151116T044328_20151116T044353_008625_00C3FA_A379.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20151104T044348_20151104T044413_008450_00BF20_85F1.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20151104T044323_20151104T044348_008450_00BF20_2670.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20151023T044348_20151023T044413_008275_00BA8E_5061.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20151023T044323_20151023T044348_008275_00BA8E_A34F.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20151011T044348_20151011T044413_008100_00B5B6_B4B2.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20151011T044323_20151011T044348_008100_00B5B6_3B75.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150929T044348_20150929T044413_007925_00B119_B722.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150929T044323_20150929T044348_007925_00B119_A440.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150917T044348_20150917T044413_007750_00AC5E_550E.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150917T044323_20150917T044348_007750_00AC5E_3A3E.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150824T044347_20150824T044412_007400_00A2E5_003F.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150824T044322_20150824T044347_007400_00A2E5_0522.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150812T044346_20150812T044411_007225_009E22_E0FB.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150812T044321_20150812T044346_007225_009E22_3C43.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150707T044344_20150707T044409_006700_008F67_7733.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150707T044319_20150707T044344_006700_008F67_89B6.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150625T044345_20150625T044410_006525_008ABB_A7F1.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150625T044320_20150625T044345_006525_008ABB_5624.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150613T044344_20150613T044409_006350_0085C3_6E36.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150613T044319_20150613T044344_006350_0085C3_D8E1.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150601T044343_20150601T044408_006175_0080A1_8315.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150601T044318_20150601T044343_006175_0080A1_F565.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150520T044323_20150520T044348_006000_007BC5_A614.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150508T044322_20150508T044347_005825_0077E8_DAC9.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150426T044321_20150426T044346_005650_0073F0_97E6.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150414T044321_20150414T044346_005475_006FDF_DA08.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150402T044320_20150402T044345_005300_006B56_95A1.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150321T044320_20150321T044345_005125_00673E_4A06.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150309T044320_20150309T044345_004950_006311_DC99.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150225T044319_20150225T044344_004775_005ED1_EC04.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150201T044320_20150201T044345_004425_005698_3F54.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150120T044320_20150120T044345_004250_0052C7_EEB1.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20150108T044320_20150108T044345_004075_004ECB_F478.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20141227T044321_20141227T044346_003900_004AE6_11DE.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20141215T044321_20141215T044346_003725_0046E8_95E7.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20141203T044322_20141203T044347_003550_0042EE_9685.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20141016T044342_20141016T044407_002850_00337F_C555.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20141016T044317_20141016T044342_002850_00337F_14FD.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20141004T044342_20141004T044407_002675_002FBB_E937.zip",
                       "https://datapool.asf.alaska.edu/GRD_HD/SA/S1A_IW_GRDH_1SDV_20141004T044317_20141004T044342_002675_002FBB_9677.zip" ]


)

for i in "${arr[@]}"
 do
 	#  wget -c --content-disposition  "${i}"  --tries=20 
     		
		wget   --continue --user=<username> --password=<passwd> "${i}"  --tries=10 
	   

	echo "Downloading $i"
done