import os, sys
sys.path.append("..")

from mgr.src.mgrlib import cropProduct, calibrateProduct, applyTerrainCorrection
from snappy import ProductIO, ProductData, HashMap, GPF, Product, ProductUtils, FlagCoding, jpy
from pathlib import Path
from argparse import ArgumentParser
import os,gc,sys,numpy

# More Java type definitions required for image generation
Color = jpy.get_type('java.awt.Color')
ColorPoint = jpy.get_type('org.esa.snap.core.datamodel.ColorPaletteDef$Point')
ColorPaletteDef = jpy.get_type('org.esa.snap.core.datamodel.ColorPaletteDef')
ImageInfo = jpy.get_type('org.esa.snap.core.datamodel.ImageInfo')
ImageLegend = jpy.get_type('org.esa.snap.core.datamodel.ImageLegend')
ImageManager = jpy.get_type('org.esa.snap.core.image.ImageManager')
JAI = jpy.get_type('javax.media.jai.JAI')
RenderedImage = jpy.get_type('java.awt.image.RenderedImage')

GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis()

def saveProduct(product, path):
    ProductIO.writeProduct(product, path, 'BEAM-DIMAP')

def classifyProduct(product, outFileName, polarization, sigmaMin):
    BandDescriptor = jpy.get_type(
        'org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor')

    band1 = BandDescriptor()
    band1.name = 'class'
    band1.type = 'float32'
    band1.expression = "if Sigma0_{polarization} < {sigmaMin} then 0 else 1".format(
        polarization=polarization, sigmaMin=sigmaMin)

    band2 = BandDescriptor()
    band2.name = 'Sigma_' + polarization
    band2.type = 'float32'
    band2.expression = 'Sigma0_' + polarization

    targetBands = jpy.array(
        'org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor', 2)
    targetBands[0] = band1
    targetBands[1] = band2

    params = HashMap()
    params.put('targetBands', targetBands)
    result = GPF.createProduct('BandMaths', params, product)
    outFileName[0] += "_class"
    product.addBand(result.getBand("class"))

    return product


def generateClassificationPng(product, pngOutDir, sigmaMin):
    classBand = product.getBand('Sigma0_VV')
    points = [ColorPoint(0.0, Color.RED), ColorPoint(
        float(sigmaMin), Color.BLACK)]

    cpd = ColorPaletteDef(points)
    ii = ImageInfo(cpd)
    classBand.setImageInfo(ii)
    image_format = 'PNG'
    im = ImageManager.getInstance().createColoredBandImage(
        [classBand], classBand.getImageInfo(), 0)

    JAI.create("filestore", im,  pngOutDir + "/overlay.png", image_format)
    print("Image saved to: " + pngOutDir + "/overlay.png")

parser = ArgumentParser()
parser.add_argument("-src",  help="Path to source image", required=True)
parser.add_argument("-ops", help="Operations to perfrom on image")
parser.add_argument("-sigma", help="Min Sigma0_VV to perform classification")
parser.add_argument("-classPngOutDir", help="Output dir for classification png")
# args = parser.parse_args()

# src = args.src
# sigma = args.sigma

src = "e:/mgr/images/thesis/20181118/20181118_orbit_cal_VV_crop_spk_tc_Sigma0_VV_reprojected.dim"
outFilename = ["test"]
sigma = 0.1


wkt = "POLYGON((22.73840573112566 53.521785, 22.73840573112566 53.521785, 22.389912 53.197930, 22.73840573112566 53.197930, 22.73840573112566 53.521785))"
pol = 'VV'

out = ProductIO.readProduct(src)
# out = calibrateProduct(out, outFilename, pol)
# out = cropProduct(out, outFilename, wkt)
# out = applyTerrainCorrection(out, outFilename, pol)
# out = classifyProduct(out, outFilename, pol, sigma)
generateClassificationPng(out, "e:/mgr/images/thesis/", 0.0097)
