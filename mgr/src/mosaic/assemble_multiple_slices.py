import os, sys, psutil
from argparse import ArgumentParser
sys.path.append("C:/git/mgr")
import mgr.src.mgrlib as mgrlib

def getMemoryUsage():
    process = psutil.Process(os.getpid())
    memoryGB = int(process.memory_info().rss) / (1 << 30)
    return "Memory usage: {} GB".format(memoryGB)

def assembleMultipleSlices(pathToFileWithImagesPaths, outImageName, outImageDir, wkt, pol):
    products = []
    imagesPaths = mgrlib.readImagesPaths(pathToFileWithImagesPaths)
    outImageName +=  "_orbit_cal"
    outImageName = [outImageName]

    for imagePath in imagesPaths:
         
        product = mgrlib.readProduct(imagePath)
        product = mgrlib.applyOrbitFile(product, ["dummy"])
        product = mgrlib.calibrateProduct(product, ["dummy"], pol)
        products.append(product)
    
    mgrlib.Logger.log("Processing images: {}".format(list(imagesPaths)))

    result = mgrlib.assembleMultipleSlices(products, outImageName, pol)
    result = mgrlib.cropProduct(result, outImageName, wkt)
   
    mgrlib.Logger.log(getMemoryUsage())
    mgrlib.saveProduct2(result, outImageDir, outImageName[0])
    mgrlib.Logger.log(getMemoryUsage())
    mgrlib.Logger.log("Image processed successfully: {}".format(os.path.join(outImageDir, outImageName[0])))

#  path = "C:/git/mgr/mgr/src/correlation/resources/images_lists/unprocessed/test_preprocess.txt"
# path = "C:/git/mgr/mgr/src/temp_images.txt"
# outImageDirPath = 'e:/mgr/unprocessed/test/preprocess/out/20141016'
# wkt = "POLYGON((22.73840573112566 53.521785, 22.73840573112566 53.521785, 22.389912 53.197930, 22.73840573112566 53.197930, 22.73840573112566 53.521785))"
# pol = 'VV'
# assembleMultipleSlices(path, "20141016",outImageDirPath,wkt, pol)

def main():
    parser = ArgumentParser()
    parser.add_argument("-pathToFileWithImagesPaths", required=True)
    parser.add_argument("-outImageDir", required=True)
    parser.add_argument("-outImageName", required=True)
    parser.add_argument("-wkt", required=True)
    parser.add_argument("-pol", required=True)
    args = parser.parse_args()

    assembleMultipleSlices(args.pathToFileWithImagesPaths, args.outImageName, args.outImageDir, args.wkt, args.pol)

if __name__ == "__main__":
    main()
