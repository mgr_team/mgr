# This scripts counts watered pixels ingiven terrain (shp file)

import os, sys, datetime, re, shutil
from argparse import ArgumentParser
sys.path.append("C:/git/mgr")
from mgr.src.mgrlib import Logger, CsvLogger, saveProduct, saveProduct2

class ProcessingImageInfo:
    def __init__(self, sigma, src, dimensions, allPixelsCount, wateredPixelsCount):
        self.sigma = sigma
        self.dimensions = dimensions
        self.src = src
        self.wateredPixelsCount = wateredPixelsCount
        self.allPixelsCount = allPixelsCount

    def __str__(self):
        return "Path:\t\t{}\nDimensions:\t{}\nAll pixels:\t{}\nWatered Pixels:\t{}".format(self.src, self.dimensions, self.allPixelsCount, self.wateredPixelsCount)

    def toCsvRow(self):
        return "{},{},{},{},{}\n".format(self.wateredPixelsCount, str(round(self.sigma,10)),  self.allPixelsCount, self.dimensions, self.src)

def addVectorToProduct(source, outFileName, vectorPath):
    from snappy import GPF, HashMap
    # To build from wkt see snappy examples -> snappy_geo_roi
    params = HashMap()
    params.put('vectorFile', vectorPath)
    params.put('separateShapes', False)
    result = GPF.createProduct("Import-Vector", params, source)
    outFileName[0] += "_vec"
    
    return result

def addRequiredBandsForWateredPixelsCounting(source, outFileName, polarization, sigmaMax):
    from snappy import GPF, HashMap, jpy
    GPF.getDefaultInstance().getOperatorSpiRegistry().loadOperatorSpis()

    BandDescriptor = jpy.get_type(
        'org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor')
    band1 = BandDescriptor()
    band1.name = 'filtered'
    band1.type = 'float64'
    band1.expression = 'geometry_test_Polygon  && Sigma0_{} < {} ? 1 : 0'.format(polarization,sigmaMax)

    band2 = BandDescriptor()
    band2.name = 'Sigma0_' + polarization
    band2.type = 'float64'
    band2.expression = 'Sigma0_' + polarization

    targetBands = jpy.array(
        'org.esa.snap.core.gpf.common.BandMathsOp$BandDescriptor', 2)
    targetBands[0] = band1
    targetBands[1] = band2

    params = HashMap()
    params.put('targetBands', targetBands)
    result = GPF.createProduct('BandMaths', params, source)
    outFileName[0] += "_mask"
   
    return result


def countWateredPixelsInProduct(product, sigmaMax, polarization):
    import numpy
    import os

    width = product.getSceneRasterWidth()
    height = product.getSceneRasterHeight()
    maskBand = product.getBand("filtered")

    wateredPixels = numpy.zeros(width, dtype=numpy.float64)
    maskPixels = numpy.zeros(width, dtype=numpy.float64)
    wateredPixels = 0

    for y in range(height):
        maskBand.readPixels(0, y, width, 1, maskPixels)

        # filtered pixels that are not zeroed by mask and smaller than sigma
        wateredPixels += (maskPixels > 0)
    wateredPixelsCount = wateredPixels.sum()
    return wateredPixelsCount


def createProcessingImageInfo(sigma, src, product, wateredPixelsCount):
    width = product.getSceneRasterWidth()
    height = product.getSceneRasterHeight()
    dimensions = "{}x{}".format(width, height)
    allPixelsCount = str(width*height)
    return ProcessingImageInfo(sigma, src, dimensions, allPixelsCount, wateredPixelsCount)

def determineOutPath(outDir, outFileName):
    path = os.path.join(outDir, outFileName)
    return path

# Method appends processingInfo to file under processedImageDatailsPath 
def processImage(src, sigmaMax, processedImageDatailsPath, polarization, outDir = None):
    from snappy import ProductIO
    
    out = ProductIO.readProduct(src)
    outFileName = [out.getName()]
    vectorPath = 'C:/git/mgr/mgr/src/correlation/resources/geometry_test_Polygon.shp'
    # Preprocessing for new big images
    # out = lib.calibrateProduct(out, outFileName, "VV")
    # out = lib.cropProduct(out, outFileName, wkt)
    # out = lib.applyTerrainCorrection(out, outFileName, "VV")
    # out = lib.reprojectToEPSG2180(out)
    # Logger.printWithNowTimePrefix("Processing image with path {}".format(src))
    out = addVectorToProduct(out, outFileName, vectorPath)
    out = addRequiredBandsForWateredPixelsCounting(out, outFileName, polarization, sigmaMax)
    wateredPixelsCount = countWateredPixelsInProduct(out, sigmaMax, polarization)
    
    processingInfo = createProcessingImageInfo(sigmaMax, src, out, wateredPixelsCount)
    Logger.log("\n" + str(processingInfo))

    if processedImageDatailsPath is not None:
            CsvLogger.writeRowToFile(processedImageDatailsPath,processingInfo.toCsvRow())
    
    if outDir is not None:
        outPath = determineOutPath(outDir, outFileName[0])
        saveProduct(out, outPath)
    return wateredPixelsCount
    
 

# Uncomment this section and comment def main to run locally
# sigma = 0.0097 
# src = 'e:/mgr/images/thesis/20170918/20170918_orbit_cal_VV_crop_spk_tc_Sigma0_VV_reprojected.dim'

# processedImageDatailsPath = os.path.abspath(os.path.dirname(__file__)) + "/processedImageDatails.csv"
# processImage(src, sigma, processedImageDatailsPath, "VV", 'e:/mgr/images/thesis/')

def main():
    parser = ArgumentParser()
    parser.add_argument("-src",  help="Path to source image", required=True)
    parser.add_argument("-sigma", help="Max sigma to perform watered pixel counting", required=True)
    parser.add_argument("-polarization", help="Polarization", required=True)
    parser.add_argument("-processedImageDatailsPath", help="Path to file for processed image datail. Not required",required=False)

    args = parser.parse_args()
    return processImage(args.src, float(args.sigma), args.processedImageDatailsPath, args.polarization)


if __name__ == "__main__":
    main()

