
import os, numpy, sys, re, pyxl
from itertools import groupby
sys.path.append("C:/git/mgr")
import mgr.src.mgrlib as mgrlib

class FlowsRecord:
    def __init__(self, date, value):
        self.date = date
        self.value = value

    def __str__(self):
        return "Date: {} Value: {}".format(self.date, self.value)

def readFlowsRecords(self):
    
    wb = pyxl.load_workbook(self.excelPath)
    ws = wb.get_active_sheet()

    records = []
    for row in ws.iter_rows(None, 2, None):
        # print("{} {}".format(row[0].value, row[1].value))
        record = FlowsRecord(row[0].value.date(), row[1].value)
        records.append(record)


records = readFlowsRecords()
grouping = groupby(records, lambda record: record.date)

with(open('C:/git/mgr/mgr/src/correlation/utils/grouped.csv', 'w')) as file:

    for k, v in grouping:
        values = list(v)
        flows = []

        for value in values:
            if not 'NA' in value:
                flow = value.split("\t")[1]
                flows.append(float(flow))
        if len(flows) == 0:
            print("No value for date {}".format(k))
        else:
            avgFlow = sum(flows)/len(flows)
            file.write('{},{}\n'.format(k,str(round(avgFlow,3)))) 
