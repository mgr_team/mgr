import os, re
from itertools import islice

def getCurrentPathWith(path):
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), path)



lines = list(islice(readLines(), 1, None))

for line in lines:
    date = re.findall(r'\d+\/\d+\/\d\d\d\d', line)[0]
    flow = line.rfind(',')

print(lines)
