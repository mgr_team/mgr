import os, sys, shutil
sys.path.append("C:/git/mgr")
from mgr.src.correlation.determine_correlation import CorrelationManager as CorrelationManager
import mgr.src.mgrlib as mgrlib

class Record: 
    def __init__(self, wateredPixelsCount, y, imagePath):
        self.x = wateredPixelsCount
        self.y = y
        self.imagePath = imagePath
        self.wateredToFlowRatio = y / wateredPixelsCount

    def __repr__(self):
        return "{}\t{}\t{}".format(self.x, self.y, self.imagePath)

class Set: 
    def __init__(self, correlation, records, start, end):
        self.correlation = correlation
        self.records = records
        self.start = start
        self.end = end

    def __repr__(self):
        return "{}\n{}\n{}\n{}\n{}".format(self.correlation, list(self.records), len(self.records), self.start, self.end)

def readRecords(path):
    records = []
    with open(path) as file:
        lines = file.read().splitlines()[1:]
        for line in lines:
            splitLine = line.split("\t")
            if line == '':
                continue
            flow = float(splitLine[0])
            watered = float(splitLine[1])
            src = splitLine[5]
            record = Record(flow, watered, src)
            records.append(record)
        return records


def getCurrentPathWith(path):
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), path)


expectedCorrelation = 0.70
numerousCorrelation = 0
bestSet = None
imagesPath = getCurrentPathWith("images_lists/determine_images_for_best_correlation.txt")
records = readRecords(imagesPath)
logger = mgrlib.ExtendedLogger(getCurrentPathWith("logs/best_images_for_correlation"))

for start in range(1000, 600000, 1000):
    for end in range(1500, 600000, 1000):

        if start >= end:
            break
        print("Range: {}x{}".format(start,end))
        recrodsForCorrelation = list(filter(lambda r:  start < r.wateredToFlowRatio and r.wateredToFlowRatio < end, records))
        
        if(len(recrodsForCorrelation) == 0 or len(recrodsForCorrelation) == 1  ):
                continue

        correlation = CorrelationManager.determineCorrelation(recrodsForCorrelation)
        
        if (correlation >= expectedCorrelation and len(recrodsForCorrelation) > numerousCorrelation):
            bestSet = Set(correlation, recrodsForCorrelation, start, end)
            numerousCorrelation = len(recrodsForCorrelation)


logger.log("\nBest sigma: {}\nStart: {}\nEnd: {}\nRecords count: {}\nRecords: {}".format(bestSet.correlation, bestSet.start, bestSet.end, len(bestSet.records), bestSet.records))