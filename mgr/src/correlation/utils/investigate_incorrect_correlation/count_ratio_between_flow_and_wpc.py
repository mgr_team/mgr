import os, sys
sys.path.append("C:/git/mgr")
import mgr.src.mgrlib as mgrlib
from mgr.src.correlation.determine_correlation import CorrelationRecord, CorrelationManager

class Record:
    def __init__(self, x, y, imagePath):
        self.x = x
        self.y = y
        self.imagePath = imagePath

class Anomaly: 
    def __init__(self, flow, wateredPixelsCount, ratio, imagePath):
        self.flow = flow
        self.wateredPixelsCount = wateredPixelsCount
        self.ratio = ratio
        self.imagePath = imagePath



# all 2015
records = [
Record(9.95, 461140, '20151104_044323_cal_crop_spk_tc.dim'),
Record(33.708, 628, '20150402_044320_cal_crop_spk_tc.dim'),
Record(9.747, 458769, '20150707_044319_cal_crop_spk_tc.dim'),
Record(11.98, 460815, '20150625_044320_cal_crop_spk_tc.dim'),
Record(7.791, 461563, '20150824_044322_cal_crop_spk_tc.dim'),
Record(8.776, 461107, '20150812_044321_cal_crop_spk_tc.dim'),
Record(18.512, 203, '20151116_044328_cal_crop_spk_tc.dim'),
Record(36.546, 79635, '20150309_044320_cal_crop_spk_tc.dim'),
Record(30.88, 837, '20150508_044322_cal_crop_spk_tc.dim'),
Record(8.341, 460759, '20150917_044323_cal_crop_spk_tc.dim'),
Record(11.708, 287274, '20151023_044348_cal_crop_spk_tc.dim'),
Record(9.95, 285662, '20151104_044348_cal_crop_spk_tc.dim'),
Record(9.716, 293330, '20151011_044348_cal_crop_spk_tc.dim'),
Record(41.487, 43446, '20150108_044320_cal_crop_spk_tc.dim'),
Record(11.98, 285629, '20150625_044345_cal_crop_spk_tc.dim'),
Record(9.747, 285089, '20150707_044344_cal_crop_spk_tc.dim'),
Record(9.716, 466661, '20151011_044323_cal_crop_spk_tc.dim'),
Record(7.791, 286006, '20150824_044347_cal_crop_spk_tc.dim'),
Record(29.792, 654, '20150520_044323_cal_crop_spk_tc.dim'),
Record(34.282, 50184, '20150225_044319_cal_crop_spk_tc.dim'),
Record(13.19, 460155, '20150613_044319_cal_crop_spk_tc.dim'),
Record(38.801, 14451, '20150201_044320_cal_crop_spk_tc.dim'),
Record(35.452, 32323, '20150321_044320_cal_crop_spk_tc.dim'),
Record(9.977, 287583, '20150929_044348_cal_crop_spk_tc.dim'),
Record(21.333, 253, '20151222_044327_cal_crop_spk_tc.dim'),
Record(24.531, 285831, '20150601_044343_cal_crop_spk_tc.dim'),
Record(36.955, 7374, '20150414_044321_cal_crop_spk_tc.dim'),
Record(11.708, 459696, '20151023_044323_cal_crop_spk_tc.dim'),
Record(36.872, 37608, '20150120_044320_cal_crop_spk_tc.dim'),
Record(8.776, 286742, '20150812_044346_cal_crop_spk_tc.dim'),
Record(13.19, 286044, '20150613_044344_cal_crop_spk_tc.dim'),
Record(8.341, 286238, '20150917_044348_cal_crop_spk_tc.dim'),
Record(18.618, 18670, '20151128_044328_cal_crop_spk_tc.dim'),
Record(9.977, 462311, '20150929_044323_cal_crop_spk_tc.dim'),
Record(32.915, 7480, '20150426_044321_cal_crop_spk_tc.dim'),
Record(24.531, 460902, '20150601_044318_cal_crop_spk_tc.dim')
                                                                                                        

]

minRatio = 1000
maxRatio = 5000


recordsToApplylation = []
anomalies = []

for record in records:
    ratio = record.y/record.x
    if (ratio > minRatio and ratio < maxRatio):
       recordsToApplylation.append(record)
       print("{},{},\t{}\tratio: {}".format(record.x, record.y, record.imagePath, ratio))
    else:
        anomalies.append(Anomaly(record.x, record.y,  ratio, record.imagePath,))  

correlation = CorrelationManager.determineCorrelation(recordsToApplylation)
print("correlation {}".format(correlation))

print("Anomalies")
for anomaly in anomalies:
    print("{},{},\t{}\tratio: {}".format(anomaly.flow, anomaly.wateredPixelsCount, anomaly.imagePath, anomaly.ratio))