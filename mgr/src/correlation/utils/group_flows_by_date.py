
import os, numpy, sys, re
from itertools import groupby
sys.path.append("C:/git/mgr")
import mgr.src.mgrlib as mgrlib


def listContainsNA(l):
    for item in l:
        if 'NA' in item:
            return True
    return False

path = 'C:/git/mgr/mgr/src/correlation/utils/flows_to_group.txt'
lines = mgrlib.readLines(path)
grouping = groupby(lines, lambda line: re.findall(r'\d+\/\d+\/\d\d\d\d', line)[0])

with(open('C:/git/mgr/mgr/src/correlation/utils/grouped.csv', 'w')) as file:

    for k, v in grouping:
        values = list(v)
        flows = []

        for value in values:
            if not 'NA' in value:
                flow = value.split("\t")[1]
                flows.append(float(flow))
        if len(flows) == 0:
            print("No value for date {}".format(k))
        else:
            avgFlow = sum(flows)/len(flows)
            file.write('{},{}\n'.format(k,str(round(avgFlow,3)))) 
