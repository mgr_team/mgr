Function RMSE(ComputedR As Range, ExpectedR As Range) As Double

errorsSquaresSum = 0
For i = 1 To ComputedR.Cells.Count
    errorsSquaresSum = errorsSquaresSum + (ComputedR(i) - ExpectedR(i)) * (ComputedR(i) - ExpectedR(i))
Next i
RMSE = Math.Sqr(errorsSquaresSum / ComputedR.Cells.Count)
End Function

