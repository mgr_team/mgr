from os.path import dirname, abspath
from itertools import groupby
import os, sys, subprocess, datetime, re, time, glob
sys.path.append("C:/git/mgr/")
import mgr.src.mgrlib as mgrlib
from mgr.src.mgrlib import Logger

class ImagesProcessor:
    def __init__(self, imagesPath, outDir, pol, wkt, sourceBands, logPath):
        self.imagesPath = imagesPath
        self.outDir = outDir
        self.pol = pol
        self.wkt = wkt
        self.sourceBands = sourceBands
        self.logger = mgrlib.ExtendedLogger(logPath, "_preprocess_images_log")

    def preprocessImage(self, imagePath, outImageDir, outImageName):
        args = ["py", "C:\git\mgr\mgr\src\correlation\preprocess_image.py", 
        "-imagePath",  imagePath,
        "-outImageDir", outImageDir,
        "-outImageName", outImageName,
        "-wkt", self.wkt,
        "-pol", self.pol]

        fail = subprocess.call(args)

        if fail:
            message = "Error occured while preprocessing product: {}\n".format(imagePath)
            self.logger.error(message)
            return False
        else:
            self.logger.log("Image preprocessed succesfully: {}".format(outImageName))
            return True
    def assembleSlices(self, imagesPaths, outImageDir, outImageName):
        imagesPath = getCurrentPathWith("temp_images.txt")
        mgrlib.saveImagesPathsToFile(imagesPath, imagesPaths)

        args = ["py", "C:/git/mgr/mgr/src/mosaic/assemble_multiple_slices.py", 
        "-pathToFileWithImagesPaths",  imagesPath,
        "-outImageDir", outImageDir,
        "-outImageName", outImageName,
        "-wkt", self.wkt,
        "-pol", self.pol]

        fail = subprocess.call(args)

        if fail:
            message = "Error occured while assembling products: {}\n".format(imagesPaths)
            self.logger.error(message)
            return False
        else:
            self.logger.log("Image preprocessed succesfully: {}".format(outImageName))
            return True

    def processImage(self, imagePath, outImageDir, outImageName, sourceBands):
        self.logger.log("Processing image with path: {}".format(imagePath))
        args = ["py", "C:/git/mgr/mgr/src/correlation/process_image.py", 
        "-imagePath",  imagePath,
        "-outImageDir", outImageDir,
        "-outImageName", outImageName,
        "-sourceBands", sourceBands]

        fail = subprocess.call(args)
        if fail:
            message = "Error occured while processing image with path: {}".format(imagePath)
            self.logger.error(message)
        else:
            self.logger.log("Image processed successfuly: {}".format(outImageName))
        time.sleep(10)

    def process(self):
       
        processedImagesDir = os.path.join(self.outDir, "process")
        preprocessedImagesDir = os.path.join(self.outDir, "preprocess")
        
        self.logger.log("Processing started")
        self.logger.log("Processing images to: {}".format(processedImagesDir))

        allImagesPaths = mgrlib.readImagesPaths(self.imagesPath)
        groups = groupby(allImagesPaths, lambda path: re.findall(r'\d{8}', path)[0])
        count = len(list(groupby(allImagesPaths, lambda path: re.findall(r'\d{8}', path)[0])))

        counter = 1
        for k,v in groups:
            self.logger.log("Processing image {}/{}".format(counter, count))
            counter += 1
            date = self.extractDateFromImagePath(k)
            imagesPaths = list(v)

            outImageName = date
            preprocessedImageDir = os.path.join(preprocessedImagesDir,outImageName)

            # todo extract preprocessing image to one method in mgrlib and
            # use it in slice

            if len(imagesPaths) > 1:
                result = self.assembleSlices(imagesPaths, preprocessedImageDir, outImageName)
            else:
                result = self.preprocessImage(imagesPaths[0], preprocessedImageDir, outImageName)
            
            time.sleep(15)
            if result:
                processedImageDir = os.path.join(processedImagesDir,outImageName)
                imagePathToProcess = self.getImageNameToProcess(preprocessedImageDir)
                imageNameToProcess = mgrlib.getFileName(imagePathToProcess)
                
                self.processImage(imagePathToProcess, processedImageDir, imageNameToProcess, self.sourceBands)

    def extractDateFromImagePath(self, imagePath):
        date = re.findall("\d{8}", imagePath)[0]
        return date.replace('T', '_')

    def getImageNameToProcess(self, outImageDir):
        path = os.path.join(outImageDir, "*.dim")
        return glob.glob(path)[0]


def getCurrentPathWith(path):
    return os.path.join(dirname(os.path.abspath(__file__)), path)


def main():
    outDir = "g:/mgr/153_test"
    pol = 'VH'
    wkt = "POLYGON((22.73840573112566 53.521785, 22.73840573112566 53.521785, 22.389912 53.197930, 22.73840573112566 53.197930, 22.73840573112566 53.521785))"
    sourceBands = "Sigma0_VH"
    imagesPath = getCurrentPathWith("resources/images_lists/unprocessed/test_images.txt")
    logPath = getCurrentPathWith("logs/process/")

    imagesProcessor = ImagesProcessor(imagesPath, outDir, pol, wkt, sourceBands, logPath)
    imagesProcessor.process()


if __name__ == "__main__":
    main()
