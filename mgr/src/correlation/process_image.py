import os, sys
sys.path.append("C:/git/mgr/")
import mgr.src.mgrlib as mgrlib
from argparse import ArgumentParser


def process(imagePath, outImageDir, outImageName, sourceBands):
    outImageName = [outImageName]
    mgrlib.Logger.log("Processing product: {}".format(imagePath))
    result = mgrlib.readProduct(imagePath)
    # mgrlib.Logger.log("Finished reading product")
    result = mgrlib.processImage(result, outImageName, sourceBands)
    # mgrlib.Logger.log("Finished processing product")
    mgrlib.printMemoryUsage()
    mgrlib.Logger.log("Saving product: {}".format(outImageName[0]))
    mgrlib.saveProduct2(result, outImageDir, outImageName[0])
    mgrlib.printMemoryUsage()

def main():
    parser = ArgumentParser()
    parser.add_argument("-imagePath",  help="Path to source image", required=True)
    parser.add_argument("-outImageDir",  help="Path to destination dir", required=True)
    parser.add_argument("-outImageName",  help="Out image name", required=True)
    parser.add_argument("-sourceBands",  help="Source bands", required=True)
    args = parser.parse_args()
    return process(args.imagePath, args.outImageDir, args.outImageName, args.sourceBands)

if __name__ == "__main__":
    main()