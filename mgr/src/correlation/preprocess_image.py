import os, sys
sys.path.append("C:/git/mgr/")
import mgr.src.mgrlib as mgrlib
from argparse import ArgumentParser


def process(imagePath, outImageDirPath, outImageName, wkt, pol):
    mgrlib.Logger.log("Processing image: {}".format(imagePath))
    outImageName = [outImageName]
    product = mgrlib.readProduct(imagePath)
    product = mgrlib.applyOrbitFile(product, outImageName)
    product = mgrlib.calibrateProduct(product, outImageName, pol)
    product = mgrlib.cropProduct(product, outImageName, wkt)
    mgrlib.saveProduct2(product, outImageDirPath, outImageName[0])
    mgrlib.Logger.log("Image processed successfully: {}".format( outImageName[0]))
    mgrlib.printMemoryUsage()
# imagePath = "e:/mgr/unprocessed/153/S1A_IW_GRDH_1SDV_20141016T044317_20141016T044342_002850_00337F_14FD.zip"
# outImageDir = "e:/mgr/unprocessed/test/preprocess/out/20141016/"
# outImageName = "20141016"
# wkt = "POLYGON((22.73840573112566 53.521785, 22.73840573112566 53.521785, 22.389912 53.197930, 22.73840573112566 53.197930, 22.73840573112566 53.521785))"
# pol = "VV"
# process(imagePath, outImageDir, outImageName, wkt, pol)

def main():
    parser = ArgumentParser()
    parser.add_argument("-imagePath",  help="Path to source image", required=True)
    parser.add_argument("-outImageDir",  help="Path to destination dir", required=True)
    parser.add_argument("-outImageName",  help="Out image name", required=True)
    parser.add_argument("-wkt",  help="Wkt for subset", required=True)
    parser.add_argument("-pol",  help="Polarisation", required=True)
    args = parser.parse_args()
    return process(args.imagePath, args.outImageDir, args.outImageName, args.wkt, args.pol)

if __name__ == "__main__":
    main()