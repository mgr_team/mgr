
import math, numpy, os, re, datetime, sys, subprocess, shutil, time
import openpyxl as pyxl
from os.path import dirname, abspath

sys.path.append("C:/git/mgr")
from mgr.src.mgrlib import Logger, ExtendedLogger, ProcessingItemsCounter, CsvLogger


class ImagePathToFlowRecord:
    def __init__(self, imagePath, flow):
        self.imagePath = imagePath
        self.flow = flow

class FlowsRecord:
    def __init__(self, date, value):
        self.date = date
        self.value = value

    def __str__(self):
        return "Date: {} Value: {}".format(self.date, self.value)

class CorrelationRecord:
    def __init__(self, flow, wateredPixelsCount):
        self.flow = flow
        self.wateredPixelsCount = wateredPixelsCount
    def __str__(self):
        return "{},{}".format(self.flow,self.wateredPixelsCount)

class CorrelationInfo:
    def __init__(self, maxSigma, correlation):
        self.maxSigma = maxSigma
        self.correlation = correlation
    
    def __str__(self):
        return "{},{}".format(self.correlation, self.maxSigma)

class FlowsManager:
    def __init__(self, filepath):
        self.excelPath = filepath
        self.flowsRecords = []
        self.readFlowsRecords()
    def readFlowsRecords(self):
        wb = pyxl.load_workbook(self.excelPath)
        ws = wb.get_active_sheet()

        for row in ws.iter_rows(None, 2, None):
            # print("{} {}".format(row[0].value, row[1].value))
            record = FlowsRecord(row[0].value.date(), row[1].value)
            self.flowsRecords.append(record)
        
    def flowRecordWithdateExist(self, date):
        return sum(1 for record in self.flowsRecords if record.date == date) > 0

    def getFlowRecordByDate(self, flowDate):
        flowRecordWithDate = list(filter(lambda flowRecord: flowRecord.date == flowDate, self.flowsRecords))
        if flowRecordWithDate.__len__() == 0:
            return None
        return flowRecordWithDate[0]


class ImagesManager:
    def __init__(self, imagesPath, loggerManager, processedImageDatailsPath, polarization):
        self.imagesPath = imagesPath
        self.loggerManager = loggerManager
        self.processedImageDatailsPath = processedImageDatailsPath
        self.polarization = polarization

    def getImagesPaths(self):
        with open(self.imagesPath) as file:
            content = file.read().splitlines()
            self.imagesPaths = content
            return content

    def extractDateFromImagePath(self, imagePath):
        date = re.findall("\d{8}", imagePath)[0]
        if(not date):
            
            return None
        return datetime.date(int(date[:4]), int(date[4:6]), int(date[6:8]))
    
    def readProcessedImageDatails(self):
        with open(self.processedImageDatailsPath) as file:
            content = file.read().splitlines()
            wateredPixelsCount = content[0].split(",")[0]
        return int(wateredPixelsCount), content[0]

    def processCountingWateredPixels(self, imagePath, sigmaMax):
        try:
            args = ["py", "C:\git\mgr\mgr\src\correlation\count_watered_pixels.py", 
            "-src",  imagePath,
            "-sigma", str(sigmaMax),
            "-polarization", self.polarization,
            "-processedImageDatailsPath", self.processedImageDatailsPath]
            fail = subprocess.call(args)

            if fail:
                self.loggerManager.error("Error occured while processing image with name: {}".format(os.path.basename(imagePath)))
                return None
            
            wateredPixelsCount,processedImageDatails = self.readProcessedImageDatails()
            return wateredPixelsCount,processedImageDatails

        except Exception as e: 
            self.loggerManager.error("Error occured while counting watered pixels for product: {} \n{}".format(imagePath, e))
            return None 
    
class CorrelationManager:
    def __init__(self, flowsManager, imagesManager, sigmaDefinition, loggerManager):
        self.flowsManager = flowsManager
        self.imagesManager = imagesManager
        self.sigmaDefinition = sigmaDefinition
        self.imagePathsToFlowsRecords = []
        self.loggerManager = loggerManager
        self.self = []

    # Pearson correlation coefficient

    # Ey2 can be a very big number, but to exceed float max number
    # it should be a product of 1.3407807929942596e+154 images
    # fully covered with water. Coefficient can also be counted via
    # covariance
    @staticmethod
    def determineCorrelation(records):
        n = len(records)
        Exy = sum(map(lambda record: record.flow*record.wateredPixelsCount, records))
        Ex = sum(map(lambda record: record.flow, records))
        Ey = sum(map(lambda record: record.wateredPixelsCount, records))
        Ex2 = sum(map(lambda record: record.flow*record.flow, records))
        Ey2 = sum(map(lambda record: record.wateredPixelsCount*record.wateredPixelsCount, records))

        try:
            coefficient = (n * Exy - Ex*Ey) /  math.sqrt((n*Ex2 - Ex*Ex)*(n*Ey2 - Ey*Ey))
            if math.isnan(coefficient):
                raise Exception("Correlation exceeds maximum number which is: {}".format(sys.float_info.max))
            return coefficient
        except Exception as e:
            print("ERROR: Determining correlation failed. Error: {}".format(e))
            return float("nan")

    
    def buildImagePathToFlowRecords(self):
        imagesPaths = self.imagesManager.getImagesPaths()

        for imagePath in imagesPaths:
            date = self.imagesManager.extractDateFromImagePath(imagePath)
            if date is None:
                self.loggerManager("Error occured while extracting date from image with path {}".format(imagePath))
                continue
            flowRecord = self.flowsManager.getFlowRecordByDate(date)

            if flowRecord is None:
                self.loggerManager.error("Flow record for date does not exist: {}".format(date))
                continue

            self.imagePathsToFlowsRecords.append(ImagePathToFlowRecord(imagePath, flowRecord.value))

    def buildCorrelationRecordsForSigma(self,sigmaMax):
        correlationRecords = []
        counter = ProcessingItemsCounter(len(self.imagePathsToFlowsRecords), self.loggerManager.messageLogger)
        for record in self.imagePathsToFlowsRecords:

            self.loggerManager.log("Image: {} Sigma: {}".format(counter.getCountingMessage(), str(round(sigmaMax, 10))))
            wateredPixelsCount, processedImageDatails = self.imagesManager.processCountingWateredPixels(record.imagePath,  sigmaMax)
            
            if wateredPixelsCount is None:
                counter.increaseCounter()
                continue

            correlationRecord = CorrelationRecord(wateredPixelsCount, record.flow)
            correlationRecords.append(correlationRecord)
            
            self.loggerManager.appendImageDatails(record.flow, processedImageDatails)
            Logger.logWithoutDate("Flow:\t\t{}\n\n".format(record.flow))
            counter.increaseCounter()
            time.sleep(5)
        return correlationRecords

    def determineNearestCorrelation(self):
        nearestCorrelation = -1
        nearestCorrelationInfo = None
        
        self.loggerManager.log("Processing started")
        self.buildImagePathToFlowRecords()

        sigmaMin = self.sigmaDefinition.sigmaMin
        sigmaMax = self.sigmaDefinition.sigmaMax
        step = self.sigmaDefinition.sigmaStep

        for maxSigma in numpy.arange(sigmaMin, sigmaMax, step):
            correlationRecords = self.buildCorrelationRecordsForSigma(maxSigma)
            correlation = self.determineCorrelation(correlationRecords)
            if correlation is None:
                continue
            if correlation > nearestCorrelation:
                nearestCorrelationInfo = CorrelationInfo(maxSigma, correlation)
            self.loggerManager.appendCorrelation(correlation, maxSigma)
            time.sleep(30)
        return nearestCorrelationInfo

class LoggerManager:
    def __init__(self, dir):
        date = datetime.datetime.now().strftime('%Y-%m-%d_%H-%M')
        self.path = os.path.join(dir, date)
       
        if os.path.exists(self.path):
            shutil.rmtree(self.path)
        os.makedirs(self.path)

        self.correlationsLogger = CsvLogger(self.path, "_correlations")
        self.imagesDatailsLogger = CsvLogger(self.path, "_images_details")
        self.messageLogger = ExtendedLogger(self.path)
        self.correlationsLogger.appendRow("Sigma, Correlation")
        self.imagesDatailsLogger.appendRow("Flows, Watered pixels count, Sigma, All pixels count, Dimensions, Src")

    def appendCorrelation(self, correlation, sigma):
        row = "{},{}".format( str(round(sigma,5)), str(round(correlation,5)))
        self.correlationsLogger.appendRow(row)

    def appendImageDatails(self, flow, processedImageDatails):
        row = "{},{}".format(flow, processedImageDatails)
        self.imagesDatailsLogger.appendRow(row)
    
    def error(self, message):
        self.messageLogger.error(message)
    
    def log(self, message):
        self.messageLogger.log(message)

class SigmaDefinition:
    def __init__(self, sigmaMin, sigmaMax, sigmaStep):
        self.sigmaMin = sigmaMin
        self.sigmaMax = sigmaMax
        self.sigmaStep = sigmaStep

def getMinutesDifference(start, stop):
    return (stop-start).total_seconds()/60

def getCurrentPathWith(path):
    return os.path.join(dirname(os.path.abspath(__file__)), path)

def main():
    start = datetime.datetime.now()
    
    imagesPath =  getCurrentPathWith("resources/images_lists/processed/VH/all.txt")
    flowsExcelPath = getCurrentPathWith("resources/flows/grouped_by_all.xlsx")
    logPath = getCurrentPathWith("results/logs")
    processedImageDetailsPath = getCurrentPathWith("results/logs/processed_image_details.csv")
    polarization = "VH"
    sigmaDefinition = SigmaDefinition(0.0032, 0.0035, 0.0001)

    loggerManager = LoggerManager(logPath)
    flowsManager = FlowsManager(flowsExcelPath)
    imagesManager = ImagesManager(imagesPath, loggerManager.path,processedImageDetailsPath, polarization)
    correlationManager = CorrelationManager(flowsManager, imagesManager, sigmaDefinition, loggerManager)
    nearestCorrelationInfo = correlationManager.determineNearestCorrelation()

    loggerManager.log("The best correlation found: {} and was determined for sigma: {}".format(
        nearestCorrelationInfo.correlation, nearestCorrelationInfo.maxSigma))
    stop = datetime.datetime.now()
    loggerManager.log("Processing finished")
    loggerManager.log("Time elapsed: {} min".format(getMinutesDifference(start, stop)))

if __name__ == "__main__":
    main()

