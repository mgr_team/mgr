import os, sys
sys.path.append("C:/git/mgr/")
import mgr.src.mgrlib as mgrlib

def process(imagePath, outImageDirPaths):
    mgrlib.Logger.log("Processing image: {}".format(imagePath))
    outImageName = [mgrlib.extractDateFromImagePath(imagePath)]
    product = mgrlib.readProduct(imagePath)
    # product = mgrlib.calibrateProduct(product, outImageName, pol)
    # product = mgrlib.cropProduct(product, outImageName, wkt)
    # product = mgrlib.applySpeckleFiltering(product, outImageName)
    # product = mgrlib.applyTerrainCorrection(product, outImageName, sourceBands)
    product = mgrlib.reprojectToEPSG2180(product, outImageName)
    mgrlib.saveProduct2(product, outImageDirPath, outImageName[0])
    mgrlib.Logger.log("Image processed successfully: {}".format( outImageName[0]))


imagePath = 'e:/mgr/processed/153_VV/20141004/20141004_orbit_cal_mosaic_crop_tc_Sigma0_VV.dim'
outImageDirPath = "e:/mgr/test/reprojection/"
process(imagePath, outImageDirPath)