import os, sys
sys.path.append("C:/git/mgr/")
import mgr.src.mgrlib as mgrlib
from argparse import ArgumentParser


def process(imagePath, outImageDirPath, outImageName, wkt, pol, sourceBands):
    mgrlib.Logger.log("Processing image: {}".format(imagePath))
    outImageName = [outImageName]
    product = mgrlib.readProduct(imagePath)
    mgrlib.Logger.log("Finished reading product")
    product = mgrlib.calibrateProduct(product, outImageName, pol)
    mgrlib.Logger.log("Finished calibration")
    product = mgrlib.cropProduct(product, outImageName, wkt)
    mgrlib.Logger.log("Finished cropping product")
    product = mgrlib.applySpeckleFiltering(product, outImageName)
    mgrlib.Logger.log("Finished applying speckle filtering")
    product = mgrlib.applyTerrainCorrection(product, outImageName, sourceBands)
    mgrlib.Logger.log("Finished applying terrain correction")

    mgrlib.Logger.log("Saving product")
    mgrlib.saveProduct2(product, outImageDirPath, outImageName[0])
    mgrlib.Logger.log("Image processed successfully: {}".format( outImageName[0]))




def getCurrentPathWith(path):
    return os.path.dirname(os.path.abspath(__file__)) +  path

path = getCurrentPathWith("/images_lists/preprocess_images.txt")
imagesPaths = mgrlib.readImagesPaths(path)

outImageDirPath = 'e:/mgr/test/processed/'
wkt = "POLYGON((22.73840573112566 53.521785, 22.73840573112566 53.521785, 22.389912 53.197930, 22.73840573112566 53.197930, 22.73840573112566 53.521785))"
pol = 'VV'
sourceBands = 'Sigma0_VV'

for imagePath in imagesPaths:
    outImageName = mgrlib.extractDateFromImagePath(imagePath)
    process(imagePath, outImageDirPath, outImageName, wkt, pol, sourceBands)
