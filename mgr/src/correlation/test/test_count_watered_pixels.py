
import os, sys, subprocess
sys.path.append("C:/git/mgr/")
import mgr.src.mgrlib as mgrlib
from mgr.src.mgrlib import Logger
from os.path import dirname

def getCurrentPathWith(path):
    return os.path.join(dirname(os.path.abspath(__file__)), path)

def processCountingWateredPixels(self, imagePath, sigmaMax):
    try:
        args = ["py", "C:\git\mgr\mgr\src\correlation\count_watered_pixels_optim.py", 
        "-src",  imagePath,
        "-sigma", str(sigmaMax),
        "-polarization", self.polarization,
        "-processedImageDatailsPath", self.processedImageDatailsPath]
        fail = subprocess.call(args)

        if fail:
            self.loggerManager.error("Error occured while processing image with name: {}".format(os.path.basename(imagePath)))
            return None
        
        wateredPixelsCount,processedImageDatails = self.readProcessedImageDatails()
        return wateredPixelsCount,processedImageDatails

    except Exception as e: 
        self.loggerManager.error("Error occured while counting watered pixels for product: {} \n{}".format(imagePath, e))
        return None 

imagesPath = "C:\git\mgr\mgr\src\correlation\resources\images_lists\processed\VV\all.txt"
imagesPaths = mgrlib.readImagesPaths(imagesPath)
cnt = 1

for imagePath in imagesPaths:
    start = datetime.datetime.now()
    mgrlib.Logger.log("Image {}/{}").format(cnt, len(imagesPaths))
    processCountingWateredPixels(imagePath, 0.0111)