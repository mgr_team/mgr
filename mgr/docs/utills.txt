Determining watered pixels manually in SNAP workflow
    1. Preprocess images
        1. Calibrate
        2. Crop
        3. TC
    2. Add polygon
    3. Add band with math expressiong Sigma0_VV < 0.01 && geometry
    4. Add mask 
    5. Polygons
        // vector
        53.49078546374467
        22.453231665692396
        53.26584727401078
        22.67917314281973
        // crop
        53.521785
        22.389912
        53.197930
        22.738405